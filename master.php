<?php
error_reporting(E_ALL ^ E_NOTICE);
function cabecera(){echo "
    <!DOCTYPE html>
    <html lang='en'>

    <head>

        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <meta http-equiv='X-UA-Compatible' content='ie=edge'>

        <!--bootstrap CSS-->
        <link rel='stylesheet' href='css/bootstrap.min.css'>
        <!--iconStyle-->
        <link rel='stylesheet' href='css/iconStyle.css'>
        <!--responsiveSlides-->
        <link rel='stylesheet' href='css/responsiveslides.css'>
        <!--sly-->
        <link rel='stylesheet' href='css/sly.css'>
        <!--Animate-->
        <link rel='stylesheet' href='css/animate.css'>
        <!--Efecto hover-->
        <link rel='stylesheet' href='css/demo.css'>
        <!--Efecto hover-->
        <link rel='stylesheet' href='css/component.css'>
        <!--Estilos-->
        <link rel='stylesheet' href='css/style.css'>


        <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'>
        <link rel='icon' href='img/favicon.ico' type='image/x-icon'>


        <!--jquery-->
        <script src='js/jquery.js' type='text/javascript'></script>
        <!--bootstrap-->
        <script src='js/bootstrap.min.js' type='text/javascript'></script>
        <!--Detectar post para animaciones-->
        <script src='js/jquery.viewportchecker.js' type='text/javascript'></script>
        
        <meta property='og:url'           content='http://dapcom.cl/' />
        <meta property='og:type'          content='Dapcom' />
        <meta property='og:title'         content='Dapcom - Facturación electrónica' />
        <meta property='og:description'   content='Facturación electrónica' />
        <meta property='og:image'         content='http://dapcom.cl/img/logo.png' />


        <title>Dapcom</title>

    </head>

    <body>

";}

function nav(){echo "
    <nav class='navbar navbar-inverse navbar-static-top'>
        <div class='container'>
                <div class='navbar-left'>
                    <ul class='nav navbar-nav'>
                        <li><a target='_blank' href='https://www.facebook.com/profile.php?id=100018834670232&fref=nf&pnref=story'><span class='icon-facebook'></span></a></li>
                        <li><a target='_blank' href='https://www.twitter.com/dapcomcl/'><span class='icon-twitter'></span></a></li>
                        <li><a target='_blank' href='https://www.instagram.com/dapcomcl/'><span class='icon-instagram'></span></a></li>
                    </ul>
                </div>

                <div class='navbar-right'>
                    <!--<div class='form-group'>
                        <span class='icon-search searchIcon'></span>
                        <input type='text' class='form-control search' placeholder='Search'>
                    </div>
                     Button trigger modal -->
                     <!--
                    <button type='button' class='btn btnMenu' data-toggle='modal' data-target='#buscar'>
                        <span class='icon-search searchIcon'></span>
                        Buscar
                    </button>
                    -->
                    <!-- Button trigger modal -->
                    <button type='button' class='btn btnMenu' data-toggle='modal' data-target='#menu'>
                        <span class='icon-menu searchIcon'></span>
                        Menu
                    </button>


                   <button type='button' class='btn btnMenu2' data-toggle='modal' data-target='#menu'>
                       <img class='btnMenuLogo' src='img/btnMenu.png' alt=''>
                   </button>
                   <button type='button' class='btn btnMenu2' data-toggle='modal' data-target='#buscar'>
                       <span class='icon-search searchIcon'></span>
                   </button>
                </div>

        </div><!-- /.container-fluid -->
    </nav>

";}

function modal(){echo "
    <!-- Modal- MENU -->
    <div class='modal fade' id='menu' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
      <div class='modal-dialog' role='document'>
        <div class='modal-content'>
          <div class='modal-header'>
            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            <div class='container-fluid'>
                <div class='col-md-12 text-right redesMenu'>
                    <a target='_blank' href='https://www.facebook.com/profile.php?id=100018834670232&fref=nf&pnref=story'><span class='icon-facebook'></span></a>
                    <a target='_blank' href='https://www.twitter.com/dapcomcl/'><span class='icon-twitter'></span></a>
                    <a target='_blank' href='https://www.instagram.com/dapcomcl/'><span class='icon-instagram'></span></a>
                </div>
            </div>
          </div>
          <div class='modal-body'>
              <div class='container-fluid'>
                  <div class='col-xs-12'>
                      <div class='col-xs-6 text-center'>
                        <img class='logoMenu' src='img/logo.png' alt=''>
                    </div>
                      <div class='col-xs-6 text-center'>
                          <a href='novaOne.php'>
                              <img src='img/novaOne.png' class='img-responsive novaOneMenu' alt=''>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
          <div class='modal-footer'>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal- BUSCAR -->
    <div class='modal fade' id='buscar' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>
      <div class='modal-dialog' role='document'>
        <div class='modal-content'>
          <div class='modal-header'>
            <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
            <h4 class='modal-title' id='myModalLabel'>Buscar</h4>
          </div>
          <div class='modal-body'>
              <div class='form-group'>
                  <input type='text' class='form-control search' placeholder='Buscar'>
              </div>
          </div>
          <div class='modal-footer'>
            <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
            <button type='button' class='btn btn-success'>Buscar</button>
          </div>
        </div>
      </div>
    </div>
";}

function footer(){echo "
    <footer>


        <div class='footerContent'>
            <div class='container post'>
              <div class='row'>
                  <div class='col-md-6'>
                        <h2>Redes sociales</h2>
                        <div class='col-md-12 redesMenu'>
                            <a target='_blank' href='https://www.facebook.com/profile.php?id=100018834670232&fref=nf&pnref=story'><span class='icon-facebook'></span></a>
                            <a target='_blank' href='https://www.twitter.com/dapcomcl/'><span class='icon-twitter'></span></a>
                            <a target='_blank' href='https://www.instagram.com/dapcomcl/'><span class='icon-instagram'></span></a>
                        </div>
                  </div>
                  <div class='col-md-6'>
                      <div class='contactForm'>
                          <form class='form-horizontal' data-toggle='validator' role='form' metrod='POST' id='form_index_contact'>
                              <!-- Nombre -->
                              <div class='form-group'>
                                  <div class='input-group'>
                                      <span class='input-group-addon icon-user addonIcon' id='basic-addon1'></span>
                                      <input id='nombre' name='nombre' type='text' placeholder='Nombre' class='form-control form-control2 input-lg' required>
                                  </div>
                              </div>

                              <!-- Teléfono-->
                              <div class='form-group'>
                                  <div class='input-group'>
                                      <span class='input-group-addon icon-mobile addonIcon' id='basic-addon1'></span>
                                      <input id='telf' name='telf' type='text' placeholder='Teléfono' class='form-control form-control2 input-lg' required>
                                  </div>
                              </div>

                              <!-- Text Correo-->
                              <div class='form-group'>
                                  <div class='input-group'>
                                      <span class='input-group-addon icon-envelop addonIcon'></span>
                                      <input id='correo' name='correo' type='email' placeholder='Correo' class='form-control form-control2 input-lg' data-error='Correo inválido, ejemplo: email@mail.com' required>
                                  </div>
                                  <div class='help-block witr-errors'></div>
                              </div>

                              <!-- Textarea -->
                              <div class='form-group'>
                                  <div class='col-md-12 noP'>
                                      <textarea class='form-control form-control2 input-lg' id='mensaje' name='mensaje' placeholder='Mensaje' required></textarea>
                                  </div>
                              </div>

                              <!-- Button -->
                              <div class='form-group'>
                                  <div class='col-md-4 noP'>
                                      <button id='enviar' type='submit' name='enviar' class='btn btn-success btn-lg enviarBtn'>Enviar</button>
                                  </div>
                              </div>

                              </form>

                          </div>
                  </div>
              </div>
            </div>


        </div>
        <div class='footer'>
            <div class='container text-center'>
                <span class='text-center text-muted'>Copyright © Dapcom Chile 2017</span>
            </div>

        </div>
    </footer>


    <!--responsiveslides-->
    <script src='js/responsiveslides.min.js' type='text/javascript'></script>
    <!--SLY - Plugin Slider-->
    <script src='js/plugins.js' type='text/javascript'></script>
    <!--SLY - sly Slider-->
    <script src='js/sly.min.js' type='text/javascript'></script>
    <!--SLY - horizontal Slider-->
    <script src='js/horizontal.js' type='text/javascript'></script>
    <!--Validator bootstrap-->
    <script src='js/validator.js' type='text/javascript'></script>

    <!--Efecto hover-->
    <script src='js/anime.min.js'></script>
    <script src='js/main.js'></script>


    <!-- classie.js by @desandro: https://github.com/desandro/classie -->
		<script src='js/classie.js'></script>
		<script src='js/modalEffects.js'></script>

    <!--WEB Js-->
    <script src='js/web.js' type='text/javascript'></script>

    <script>
    (function() {
        [].slice.call(document.querySelectorAll('.grid--effect-vega > .grid__item')).forEach(function(stackEl) {
            new VegaFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-castor > .grid__item')).forEach(function(stackEl) {
            new CastorFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-hamal > .grid__item')).forEach(function(stackEl) {
            new HamalFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-polaris > .grid__item')).forEach(function(stackEl) {
            new PolarisFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-alphard > .grid__item')).forEach(function(stackEl) {
            new AlphardFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-altair > .grid__item')).forEach(function(stackEl) {
            new AltairFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-rigel > .grid__item')).forEach(function(stackEl) {
            new RigelFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-canopus > .grid__item')).forEach(function(stackEl) {
            new CanopusFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-pollux > .grid__item')).forEach(function(stackEl) {
            new PolluxFx(stackEl);
        });
        [].slice.call(document.querySelectorAll('.grid--effect-deneb > .grid__item')).forEach(function(stackEl) {
            new DenebFx(stackEl);
        });
    })();
    </script>

    </body>

    </html>
";}
 ?>
