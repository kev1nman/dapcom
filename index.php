<?php require_once("master.php"); cabecera(); nav(); modal(); ?>

    <!-- sect1 -->
    <section class='sect1'>
        <!-- CONTAINER -->
        <div class='container'>
            <div class='row'>
                <a href="index.php">
                    <img class='img-responsive center-block logo animated pulse' src='img/logo.png' alt=''>
                </a>
            </div>
        </div>
        <!-- FIN / CONTAINER -->
    </section>
    <!-- FIN / sect1 -->


    <!-- sect2 -->
    <section class="sect2">
        <div class="container post">
            <div class="row">
                <h1>
                    <a href="novaOne.php">
                        <img class="img-responsive" src='img/novaOne.png' alt="">
                    </a>
                    
                </h1>
                <div class="col-md-6 separador noP">
                    
                    <p>
                        Potente solución de software, que permite gestionar y administrar toda la
                        información de los documentos tributarios, tanto manuales como electrónicos (en este
                        último caso, cumplimos con todas las normativas exigidas por el SII) para apoyar los
                        procesos de gestión de las empresas. <br><br>

                        Consta de dos poderosas herramientas que trabajan en conjunto. Aplicaciones
                        amigables, fáciles de usar, integradas con nuestra aplicación FEL portal web de
                        Facturación Electrónica. <br><br>

                        La primera es una aplicación de escritorio, que permite
                        llevar el control de los documentos electrónicos y/o físicos,
                        incluyendo documentos de gestión y configuración de
                        documentos manuales.
                    </p>

                </div>

                <div class="col-md-6">
                    
                    <ul class="text-left lista">
                        <li><a href="novaOne.php#funciones"><span class="glyphicon glyphicon-chevron-right"></span>Funciones básicas</a></li>
                        <li><a href="novaOne.php#documentos"><span class="glyphicon glyphicon-chevron-right"></span>Documentos</a></li>
                        <li><a href="novaOne.php#modulos"><span class="glyphicon glyphicon-chevron-right"></span>Módulos</a></li>
                    </ul>

                </div>
            </div>

        </div>
    </section>
    <!-- FIN / sect2 -->


    <!-- sect3 -->
    <section class='sect3'>
        <div class="container post">
            <div class="row">
                <h1 class="verticalText">¿ Quiénes somos ?</h1>
                <div class="col-md-6 flechas1">
                    <img class="img-responsive" width="150px" src="img/flecha1.png" alt="">
                    <img class="img-responsive logoDapcom" src="img/logo3.png" alt="">
                    <div class="col-xs-12 noP">
                        <img class="img-responsive" width="150px" style="float:right;" src="img/flecha2.png" alt="">
                    </div>
                    <p>
                        <br><br><br><br><br><br>
                        Para realizar las diferentes labores, nuestra empresa cuenta con un staff de ingenieros altamente especializados, que han dedicado su vida al desarrollo de softwares tributarios. Ellos están preparados y capacitados para brindar los mejores productos y servicios para sus clientes. Nuestra casa matriz y centro de capacitación están ubicados en la ciudad de Santiago.
                    </p>
                    <div class="col-xs-12 noP">
                        <img class="img-responsive" width="250px" style="float:right;" src="img/flecha4.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 paddingTop150">
                    <p>
                        Desde hace aproximadamente XXX años, un grupo de profesionales altamente preparado, nos hemos dedicado a desarrollar una plataforma tecnológica de alta calidad y velocidad de respuesta. Logrando posicionar la empresa en una nueva manera de distribución de software, adaptando día a día las novedades del mercado, para llevarlo a tus manos y así hacer más fácil tus procesos.
                    </p>
                    <div class="col-xs-12 noP">
                        <img class="img-responsive" width="250px" style="float:left;" src="img/flecha3.png" alt="">
                    </div>
                    <p>
                        <br><br><br><br>
                        Su especialización, experiencia y habilidades en el área tributaria, contable y de tecnologías de información le permite ofrecer la mejor y más económica solución para los clientes que requieran facturar electrónicamente.
                        Los productos desarrollados por nuestros profesionales son altamente operativos, especializados y cuentan con un excelente servicio de pre y post venta, a lo largo de nuestro país, estas son soluciones muy fáciles de ejecutar, adecuándose así, a cualquier tipo de cliente.
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- FIN / sect3 -->

    <!-- sect4 -->
    <section class='sect4'>
        <div class="container post">
            <div class="row">
                <div class="col-md-12 noP">
                    <h1 class="tituloSections">¿ Por qué elegirnos ?</h1>
                </div>
                <img class="girl" src="img/diplo2.png" alt="">
                <div class="col-md-8 col-md-offset-4">
                    
                    <div class="row">
                        <div class="col-md-6">
                        
                            <ul class="text-left lista">
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>Totalmente modificable.</li>
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>El Software se ajusta a la operación real de cada empresa.</li>
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>Reportes e Impresiones a la medida de la organización.</li>
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>Capacitación constante a todos nuestros clientes con soporte.</li>
                            </ul>
                        </div>

                        <div class="col-md-6">
                        
                            <ul class="text-left lista">
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Sin licencias.</li>
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Resolución de consultas de forma rápida y efectiva.</li>
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Soporte expedito via WhatsApp, Teléfono o correo Electrónico.</li>
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Reportes diarios, semanales, mensuales.</li>
                            </ul>

                        </div>
                    </div>

                    <div class="row paddingTop50">
                        <div class="col-md-6">
                        
                            <ul class="text-left lista">
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Sin licencias.</li>
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Resolución de consultas de forma rápida y efectiva.</li>
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Soporte expedito via WhatsApp, Teléfono o correo Electrónico.</li>
                                <li><span class="glyphicon glyphicon-chevron-right cyan"></span>Reportes diarios, semanales, mensuales.</li>
                            </ul>

                        </div>

                        <div class="col-md-6">
                        
                            <ul class="text-left lista">
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>Totalmente modificable.</li>
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>El Software se ajusta a la operación real de cada empresa.</li>
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>Reportes e Impresiones a la medida de la organización.</li>
                                <li><span class="glyphicon glyphicon-chevron-right verde"></span>Capacitación constante a todos nuestros clientes con soporte.</li>
                            </ul>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- FIN / sect4 -->


    <!-- sect5 -->
    <section class="sect5">
        <div class="container post">
            <div class="row">
                <div class="col-xs-12 noP">
                    <h1 class="tituloSections">Nuestros aliados</h1>
                </div>
                <div class="col-xs-6 text-center">
                    <img src="img/vapp.png" class="img-responsive" alt="">
                </div>
                <div class="col-xs-6 text-center">
                    <img src="img/neoSoftware.png" class="img-responsive" alt="">
                </div>
            </div>

        </div>
    </section>
    <!-- FIN / sect5 -->








    <!-- sect6 -->
    <section class='sect8'>
        <div class="container post">
            <div class="row">
                <h1>
                    Team viewer
                </h1>
                <br>
                <div class="col-md-6 separador noP">
                    
                    <p>
                        Potente solución de software, que permite gestionar y administrar toda la
                        información de los documentos tributarios, tanto manuales como electrónicos (en este
                        último caso, cumplimos con todas las normativas exigidas por el SII) para apoyar los
                        procesos de gestión de las empresas. <br><br>

                        Consta de dos poderosas herramientas que trabajan en conjunto. Aplicaciones
                        amigables, fáciles de usar, integradas con nuestra aplicación FEL portal web de
                        Facturación Electrónica. <br><br>

                        La primera es una aplicación de escritorio, que permite
                        llevar el control de los documentos electrónicos y/o físicos,
                        incluyendo documentos de gestión y configuración de
                        documentos manuales.
                    </p>

                </div>

                <div class="col-md-6 text-center">

                    <a href="#">
                        <img src="img/teamViewer.png" class="img-responsive teamViewer" alt="">
                    </a>
                    
                    

                    <a href="#" class='btn btn-success bgAzul btn-lg enviarBtn'>Descargar</a>

                </div>
            </div>

        </div>
    </section>
    <!-- FIN / sect6 -->


<?php footer(); ?>
