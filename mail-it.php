<?php

/* Code by David McKeown - craftedbydavid.com */
/* Editable entries are bellow */

$send_to = "kevinjmr@gmail.com";
$send_subject = "DAPCOM - Constacto en footer";

/*Be careful when editing below this line */

//Variables del formulario
$nombre = cleanupentries($_POST["nombre"]);
$telefono = cleanupentries($_POST["telf"]);
$correo = cleanupentries($_POST["correo"]);
$mensaje = cleanupentries($_POST["mensaje"]);

function cleanupentries($entry) {
	$entry = trim($entry);
	$entry = stripslashes($entry);
	$entry = htmlspecialchars($entry);

	return $entry;
}

//Cuerpo del correo
$message = "Este correo fue enviado el " . date('m-d-Y') .
"\nNombre: " . $nombre .
"\n\nCorreo: " . $correo .
"\nTelefono: " . $telefono .
"\nMensaje: " . $mensaje .

$send_subject .= " - {$nombre}";

$headers = "From: " . $correo . "\r\n" .
    "Reply-To: " . $correo . "\r\n" .
    "X-Mailer: PHP/" . phpversion();

if (!$nombre) {
	echo "Completa todos los campos (Error de nombre!)";
	exit;
}else if (!$telefono){
	echo "Completa todos los campos (Error de teléfono!)";
	exit;
}else if (!$correo){
	echo "Completa todos los campos (Error de correo!)";
	exit;
}else if (!$mensaje){
	echo "Completa todos los campos (Escribe un mensaje!)";
	exit;
}else{
	if (filter_var($correo, FILTER_VALIDATE_EMAIL)) {
		mail($send_to, $send_subject, $message, $headers);
	 	echo "Gracias por contactarnos, pronto nos estaremos comunicando con usted.";
	}else{
		echo "Ups! Parece que hubo un error, por favor inténtalo de nuevo!";
		exit;
	}
}

?>
