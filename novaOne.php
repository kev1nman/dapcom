<?php require_once("master.php"); cabecera(); nav(); modal(); ?>
<!-- sect1 -->
<section class='sect1One'>
    <!-- CONTAINER -->
    <div class='container'>
        <div class='row'>
            <div class='col-md-12 text-center'>
                <a href="index.php">
                    <img class='img-responsive center-block logo animated pulse' src='img/logo.png' alt=''>
                </a>
            </div>
        </div>
        <div class="row logosNova">
            <div class="col-md-6 text-center">
                <img class="img-responsive" src='img/novaGes.png' alt="">
            </div>
            <div class="col-md-6 text-center">
                <img class="img-responsive" src='img/novaAdm.png' alt="">
            </div>
        </div>
    </div>
    <!-- FIN / CONTAINER -->
</section>
<!-- FIN / sect1 -->


<!-- sect2 -->
<section class="sect2">
    <div class="container post">
        <div class="row">
            <div class="col-md-12 noP">
                <h1>
                    <img class="img-responsive" src='img/novaOne.png' alt="">
                </h1>
                <p>
                    Potente solución de software, que permite gestionar y administrar toda la
                    información de los documentos tributarios, tanto manuales como electrónicos (en este
                    último caso, cumplimos con todas las normativas exigidas por el SII) para apoyar los
                    procesos de gestión de las empresas. <br><br>

                    Consta de dos poderosas herramientas que trabajan en conjunto. Aplicaciones
                    amigables, fáciles de usar, integradas con nuestra aplicación FEL portal web de
                    Facturación Electrónica. <br><br>

                    La primera es una aplicación de escritorio, que permite
                    llevar el control de los documentos electrónicos y/o físicos,
                    incluyendo documentos de gestión y configuración de
                    documentos manuales.
                </p>

            </div>
        </div>

    </div>
</section>
<!-- FIN / sect2 -->


<!-- sect3 -->
<section class='sect3 sect3One' id="documentos">
    <div class="container post">
        <div class="row">
            <div class="col-md-12 noP">

                <a class="" href="novaOne.php">
                    <div class="iconOne">
                        <img class="img-responsive" src="img/iconOne.png" class="" alt="">
                    </div>
                </a>
                <h1 class="tituloSections">Abarca <small>los siguientes documentos</small></h1>
                <div class="row">
                    <div class="col-md-4">
                        <div class="separador">
                            <h3>Tributario electrónicos</h3>
                            <ul class="listaOne">
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Factura electrónica</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Factura exenta electrónica</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Nota crédito electrónica</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Nota de débito electrónica</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Guía despacho electrónica</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="separador">
                            <h3>Documentos tributarios manuales</h3>
                            <ul class="listaOne">
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Factura</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Factura exenta</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Nota crédito</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Nota de débito</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Guía despacho</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Totales boletas (libro ventas)</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="separador">
                            <h3>Documentos no tributarios</h3>
                            <ul class="listaOne">
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Cotización</li>
                                <li><span class="glyphicon glyphicon-chevron-right"></span> Nota de Venta</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<!-- FIN / sect3 -->

<!-- MODAL / sect4One -->

<!-- Clientes -->
<div class="md-modal md-effect-11" id="clientes">
    <div class="md-content">
        <h3>Manejo de clientes</h3>
        <div>
            <div class="col-md-6 text-center noP">
                <img class="img-responsive" src="img/ventana1.jpg" alt="">
            </div>
            <div class="col-md-6 modalDescripcion">
                <p>Se puede registrar toda la data relacionada con el
cliente, bien sea facturador electrónico o no.</p>
            </div>
            <button class="btn btn2 btn-success md-close" type="button" name="button">Cerrar</button>
        </div>
    </div>
</div>
<!-- Vendedores -->
<div class="md-modal md-effect-11" id="vendedores">
    <div class="md-content">
        <h3>Manejo de vendedores</h3>
        <div>
            <div class="col-md-6 noP">
                <img class="img-responsive" src="img/ventana4.jpg" alt="">
            </div>
            <div class="col-md-6 modalDescripcion">
                <p>Registra los datos relacionados con los vendedores de la
empresa, acredita al mismo a la utilización del sistema en forma de vendedor,
teniendo la opción de emitir Notas de Ventas.</p>
            </div>
            <button class="btn btn2 btn-success md-close" type="button" name="button">Cerrar</button>
        </div>
    </div>
</div>
<!-- Productos -->
<div class="md-modal md-effect-11" id="productos">
    <div class="md-content">
        <h3>Manejo de productos</h3>
        <div>
            <div class="col-md-6 noP">
                <img class="img-responsive" src="img/ventana3.jpg" alt="">
            </div>
            <div class="col-md-6 modalDescripcion">
                <p>Donde puedes dar la definición de Productos
y Servicios, incluyendo asociación de Familias, marcas, sub-Familias, Unidades,
Precios, descuentos, Codificación, impuestos.</p>
            </div>
            <button class="btn btn2 btn-success md-close" type="button" name="button">Cerrar</button>
        </div>
    </div>
</div>
<!-- proveedores -->
<div class="md-modal md-effect-11" id="proveedores">
    <div class="md-content">
        <h3>Manejo de proveedores</h3>
        <div>
            <div class="col-md-6 noP">
                <img class="img-responsive" src="img/ventana2.jpg" alt="">
            </div>
            <div class="col-md-6 modalDescripcion">
                <p>Donde se puede registrar toda la data relacionada con el
proveedor, bien sea facturador electrónico o no.</p>
            </div>
            <button class="btn btn2 btn-success md-close" type="button" name="button">Cerrar</button>
        </div>
    </div>
</div>
<!-- configuracion -->
<div class="md-modal md-effect-11" id="configuracion">
    <div class="md-content">
        <h3>Configuración de la empresa</h3>
        <div>
            <div class="col-md-6 noP">
                <img class="img-responsive" src="img/ventana5.jpg" alt="">
            </div>
            <div class="col-md-6 modalDescripcion">
                <p>Configurar el perfil de la empresa, incluyendo logo para emisión de PDF.</p>
            </div>
            <button class="btn btn2 btn-success md-close" type="button" name="button">Cerrar</button>
        </div>
    </div>
</div>


<section class="sect4One" id="funciones">
    <div class="container-fluid post">
        <div class="row">
            <div class="col-md-12 noP">
                <h1 class="tituloSections">Cuenta con las siguientes funcionalidades básicas</h1>
                <div class="container-fluid">
                    <div class="row">
                        <!--IZQUIERDA-->
                        <div class="col-sm-6 separador">
                            <div class="circuloPunta"></div>
                            <div class="circuloPunta circuloPuntaAbajo"></div>
                            <div class="row">
                                <div class="col-xs-12 espacioCirculo">
                                    <div class="tiraCirculos">
                                        <div class="circuloVerde"></div><div class="circuloVerde circuloVerde2"></div>
                                        <div class="circuloBlanco"></div>
                                    </div>
                                    <div class="grid grid--effect-hamal circuloImgLeft">
                                        <buttom class="grid__item grid__item--c1 md-trigger" data-modal="clientes">
                                            <div class="stack">
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__figure">
                                                    <img class="stack__img" src="img/funciones.png" alt="Image"/>
                                                    <p class="tituloCirculo">Manejo de <br> <strong>clientes</strong></p>
                                                </div>
                                            </div>
                                            <div class="grid__item-caption" style="display:none;">
                                                <h3 class="grid__item-title">anaerobic</h3>
                                                <div class="column column--left">
                                                    <span class="column__text">Period</span>
                                                    <span class="column__text">Subjects</span>
                                                    <span class="column__text">Result</span>
                                                </div>
                                                <div class="column column--right">
                                                    <span class="column__text">2045</span>
                                                    <span class="column__text">133456</span>
                                                    <span class="column__text">Positive</span>
                                                </div>
                                            </div>
                                        </buttom>
                                    </div>
                                </div>
                                <div class="col-xs-12 espacioCirculo"></div><!--ESPACIO VACIO-->
                                <div class="col-xs-12 espacioCirculo">
                                    <div class="tiraCirculos">
                                        <div class="circuloVerde"></div><div class="circuloVerde circuloVerde2"></div>
                                        <div class="circuloBlanco"></div>
                                    </div>
                                    <div class="grid grid--effect-hamal circuloImgLeft">
                                        <buttom class="grid__item grid__item--c1 md-trigger" data-modal="vendedores">
                                            <div class="stack">
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__figure">
                                                    <img class="stack__img" src="img/funciones.png" alt="Image"/>
                                                    <p class="tituloCirculo">Manejo de <br> <strong>vendedores</strong></p>
                                                </div>
                                            </div>
                                            <div class="grid__item-caption" style="display:none;">
                                                <h3 class="grid__item-title">anaerobic</h3>
                                                <div class="column column--left">
                                                    <span class="column__text">Period</span>
                                                    <span class="column__text">Subjects</span>
                                                    <span class="column__text">Result</span>
                                                </div>
                                                <div class="column column--right">
                                                    <span class="column__text">2045</span>
                                                    <span class="column__text">133456</span>
                                                    <span class="column__text">Positive</span>
                                                </div>
                                            </div>
                                        </buttom>
                                    </div>
                                </div>
                                <div class="col-xs-12 espacioCirculo"></div><!--ESPACIO VACIO-->
                                <div class="col-xs-12 espacioCirculo">
                                    <div class="tiraCirculos">
                                        <div class="circuloVerde"></div><div class="circuloVerde circuloVerde2"></div>
                                        <div class="circuloBlanco"></div>
                                    </div>
                                    <div class="grid grid--effect-hamal circuloImgLeft">
                                        <buttom class="grid__item grid__item--c1 md-trigger" data-modal="productos">
                                            <div class="stack">
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__figure">
                                                    <img class="stack__img" src="img/funciones.png" alt="Image"/>
                                                    <p class="tituloCirculo">Manejo de <br> <strong>productos</strong></p>
                                                </div>
                                            </div>
                                            <div class="grid__item-caption" style="display:none;">
                                                <h3 class="grid__item-title">anaerobic</h3>
                                                <div class="column column--left">
                                                    <span class="column__text">Period</span>
                                                    <span class="column__text">Subjects</span>
                                                    <span class="column__text">Result</span>
                                                </div>
                                                <div class="column column--right">
                                                    <span class="column__text">2045</span>
                                                    <span class="column__text">133456</span>
                                                    <span class="column__text">Positive</span>
                                                </div>
                                            </div>
                                        </buttom>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!--DERECHA-->
                        <div class="col-sm-6 separador2">
                            <div class="circuloPunta circuloPuntaAbajo2"></div>
                            <div class="row">
                                <div class="col-xs-12 espacioCirculo"></div><!--ESPACIO VACIO-->
                                <div class="col-xs-12 espacioCirculo">
                                    <div class="tiraCirculosR">
                                        <div class="circuloVerde"></div><div class="circuloVerde circuloVerde2"></div>
                                        <div class="circuloBlanco"></div>
                                    </div>
                                    <div class="grid grid--effect-hamal circuloImgRight">
                                        <buttom class="grid__item grid__item--c1 md-trigger" data-modal="proveedores">
                                            <div class="stack">
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__figure">
                                                    <img class="stack__img" src="img/funciones.png" alt="Image"/>
                                                    <p class="tituloCirculo">Manejo de <br> <strong>proveedores</strong></p>
                                                </div>
                                            </div>
                                            <div class="grid__item-caption" style="display:none;">
                                                <h3 class="grid__item-title">anaerobic</h3>
                                                <div class="column column--left">
                                                    <span class="column__text">Period</span>
                                                    <span class="column__text">Subjects</span>
                                                    <span class="column__text">Result</span>
                                                </div>
                                                <div class="column column--right">
                                                    <span class="column__text">2045</span>
                                                    <span class="column__text">133456</span>
                                                    <span class="column__text">Positive</span>
                                                </div>
                                            </div>
                                        </buttom>
                                    </div>
                                </div>
                                <div class="col-xs-12 espacioCirculo"></div><!--ESPACIO VACIO-->
                                <div class="col-xs-12 espacioCirculo">
                                    <div class="tiraCirculosR">
                                        <div class="circuloVerde"></div><div class="circuloVerde circuloVerde2"></div>
                                        <div class="circuloBlanco"></div>
                                    </div>
                                    <div class="grid grid--effect-hamal circuloImgRight">
                                        <buttom class="grid__item grid__item--c1 md-trigger" data-modal="configuracion">
                                            <div class="stack">
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__deco"></div>
                                                <div class="stack__figure">
                                                    <img class="stack__img" src="img/funciones.png" alt="Image"/>
                                                    <p class="tituloCirculo">Configuración de la <br> <strong>empresa</strong></p>
                                                </div>
                                            </div>
                                            <div class="grid__item-caption" style="display:none;">
                                                <h3 class="grid__item-title">anaerobic</h3>
                                                <div class="column column--left">
                                                    <span class="column__text">Period</span>
                                                    <span class="column__text">Subjects</span>
                                                    <span class="column__text">Result</span>
                                                </div>
                                                <div class="column column--right">
                                                    <span class="column__text">2045</span>
                                                    <span class="column__text">133456</span>
                                                    <span class="column__text">Positive</span>
                                                </div>
                                            </div>
                                        </buttom>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="corteBajoOne"></div>

</section>


<!-- sect5 -->
<section class="sect5 sect5One" id="modulos">
    <div class="container post">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 noP">
                <h1 class="tituloSections cyan text-center">Dentro de estos modulos puedes realizar</h1>
                <ul>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Emisión de informes por distintos criterios (por período, cliente, vendedor,
proveedor, fecha, rango de fecha, sucursal, pendiente de pago, etc.).</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Incluye un especializado asistente de configuración de documentos manuales o
físicos.</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Definición del período contable.</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Generación de cotizaciones y notas de venta.</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Incluye guía de despacho tanto electrónica como manual.</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Facturación a partir de guías de despacho, cotizaciones y notas de venta.</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Firma electrónica de documentos.</li></li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Generación y envío de los libros de Compras, Ventas y Guías de Despacho.</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Módulo de manejo de intercambio comercial entre emisores y receptores.</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Administración de CAF (Código de Autorización de Folios).</li>
                    <li><span class="glyphicon glyphicon-chevron-right"></span>Generación de Documentos electrónicos tributarios en formato PDF (Cedibles y
Tributarios).</li>
                </ul>

            </div>
        </div>
        
    </div>
    <div class="corteBajoOne"></div>
    
</section>
<!-- FIN / sect5 -->




<div class="md-overlay"></div><!-- the overlay element -->


<?php footer(); ?>
