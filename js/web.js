//Animaciones en post
$(document).ready(function() {
    $('.post').addClass('hidden').viewportChecker({
        classToAdd: 'visible animated fadeInLeft', // Class to add to the elements when they are visible
        offset: 100
    });
});

//Valor de botones para el correo de servicios
$('.click').click(function(){
    var idBoton = $(this).attr('id');
    $('#click').attr('value',idBoton);
});

// Formulario CONTACTANOS
$(document).on('click', '.enviarBtn', function(e) {
    e.preventDefault();
    var url = 'mail-it.php';

    $.ajax({
        type: 'POST',
        url: url,
        data: $('#form_index_contact').serialize(),
        success: function(data) {
            console.log(data);
            $('#respuesta').html(data);
            $('#respuesta').slideDown();
            $('#respuesta2').modal('show');
            document.getElementById('form_index_contact').reset();
        }
    });
});

// Formulario SERVICIOS, PRODUCTOS, Boton CONTACTO en el menu y CUENTANOS TU IDEAA
$(document).on('click', '.enviarBtn2', function(e) {
    e.preventDefault();
    var url = 'mail-it-servicios.php';

    $.ajax({
        type: 'POST',
        url: url,
        data: $('#form_index_servicios').serialize(),
        success: function(data) {
            console.log(data);
            $('#respuestaServicios').html(data);
            $('#respuestaServicios').slideDown();
            //$('#respuestaServicios2').modal('show');
            document.getElementById('form_index_servicios').reset();
        }
    });
});

//Valor de botones para el correo de servicios
$('#ok').click(function(){
    $('#respuesta').hide();
    $('#respuestaServicios').hide();
});
